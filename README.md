LightSkin für IP-Symcon WebFront im WindNODE Design

Version 1.1 

* Benötigt: IP-Symcon 5.0 oder neuer
* Größe: 7.0 MB
* Farbton: weiß/grau


| Farbe      | R   | G   | B   | #HTML   |
| ---------- | ---:| ---:| ---:| ------- |
| Orange     | 255 |  90 |  40 | ![#FF5A28](https://placehold.it/15/FF5A28/000000?text=+) #FF5A28 |
| Dunkelblau |  30 | 120 | 130 | ![#1E7882](https://placehold.it/15/1E7882/000000?text=+) #1E7882 |
| Hellblau   |  90 | 170 | 180 | ![#5AAAB4](https://placehold.it/15/5AAAB4/000000?text=+) #5AAAB4 |
| Schrift    |  30 |  27 |  23 | ![#1E1B17](https://placehold.it/15/1E1B17/000000?text=+) #1E1B17 |
